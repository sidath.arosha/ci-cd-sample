FROM openjdk:8u111-jdk-alpine
ADD build/libs/ci-cd-sample.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]